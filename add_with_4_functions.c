//Write a program to add two user input numbers using 4 functions.//program to find the sum of two numbers using four functions
//the functions used are input,sum,output,main function.
#include <stdio.h>
int input()
{
    int n; 
    printf("Enter a number");
    scanf("%d",&n);
    return n;
}
int sum(int p, int q)
{
    int add;
    add = p+q;
    return add;
}
void output(int x, int y, int z)
{
    printf("Sum of %d + %d is %d\n",x,y,z);
}
int main()
{
    int a,b,c;
    a=input();
    b=input();
    c=sum(a,b);
    output(a,b,c);
    return 0;
}